\version "2.19.83"

init = {
  \key ges \major
  \time 3/4
  \clef bass
  \tempo "Andante espressivo"
}

a_upper = \relative c' {
  \init
  <<
    \relative c' {
      bes4.\( ces8 des ces | 
      \stemDown <bes ges>4. ges8 ges bes |
      aes bes16 aes f2\) |
    }
    \\
    \relative c' {ges2.}
  >>
  <<
    \relative c' {
      bes4.\( ces8 des ces | 
      \stemDown <bes ges>4. ges8 ges bes |
      aes bes16 aes f4. ges8 |
      ges4\)}
    \\
    {ges2.}
  >>
  \stemNeutral
  r8
  \clef treble
  <bes' ges> <bes ges>( <des bes> |
  <es ces>4.) <ges es>8 <ges es>( <es ces> |
  <des bes>4.) <bes ges>8 <bes ges>( <des bes> |
  <es ces>4.) <ges es>8 <ges es>--( <aes f>) |
  <aes f>2--\breathe r8 <bes ges> |
  <bes ges>4. <ges es>8 <ges es>( <bes ges>) |
  << {aes4} \\ {es8[( f)]} >> <aes f>( <f des>) <f des>( <aes f>) |
  <ges es>( <es ces>) <es ces>( <ges es>) <ges es>( <es ces>) |
  \tuplet 4/3 {<ces aes>4(^\markup{\italic{"rit."}} es des ces)} |
  \tupletNeutral
}

a_lower = \relative c {
  \init
  <<
    {des4.\( es8 f es |
    des4. bes8 bes des\) |}
    \\
    {ges,2. | ges |}
  >>
  <ges' es ces>4 <des ces>2
  <<
    {des4.\( es8 f es |
    des4. bes8 bes des\) |}
    \\
    {ges,2. | ges |}
  >>
  <ges' es ces>4 <des ces>2
  <des ges,>4 r8
  \clef treble
  <des' ges,> <des ges,>( <f bes,> |
  <ges ces,>4.) <bes es,>8 <bes es,>( <ges ces,> | 
  <f bes,>4.) <des ges,>8 <des ges,>( <f bes,> |
  <ges ces,>2) <bes es,>4-- |
  <des f,>2.-- |
  <bes es,>2. |
  <aes des,>2. |
  <ges ces,>2. |
  <es aes,>2. |
}
