\version "2.19.83"

e_upper = \relative c'' {
  \clef treble
  <<
    \relative c'' { 
      \stemNeutral
      <ges bes>4.( ces8 des ces |
      <ges bes>4. ges8 ges bes |
      \stemUp
      aes8 bes16 aes f2)
    }
    \\
    \relative c' {
      s2.*2 |
      <ces es>4 <ces des>2
    }
  >>
  |
  <<
    \relative c'' {
      \stemNeutral
      <ges bes>4.( ces8 des ces | 
      <ges bes>4. ges8 ges bes | 
      \stemUp
      \override Arpeggio.extra-spacing-width = #'(-1 . 0)
      \time 4/4
      aes8 bes16 aes f4. ges8 ges4\arpeggio |
      \time 3/4
      <bes, des ges>4.)\arpeggio 
    }
    \\
    \relative c' {
      s2.*2
      <ces es>4 <ces des>2 <ces des>8\arpeggio des16 ces
    }
  >>
  <ges bes>8 <ges bes>( <bes des> |
  <ces es>4.) <es ges>8 <es ges>( <ces es> |
  <bes des>4.) <ges bes>8 <ges bes>( <bes des> |
  <ces es>4.) <es ges>8 <es ges>--( <f aes>) |
  <f aes>2--\breathe r8 <ges bes>8 |
  <ges bes>4. <es ges>8 <es ges>( <ges bes>) |
  << {aes4} \\ {es8[( f])} >>
  <f aes>( <des f>) <des f>( <f aes>) |
  <es ges>( <ces es>) <ces es>( <es ges>) <es ges>( <ces es>) |
  <aes ces>( <ces es>) <ces es>( <aes ces>) <aes ces>( <f aes>) |
  <f aes>( <aes ces>) <aes ces>( <f aes>) <f aes>( <aes ces>) |
  <<
    \relative c' {
      bes4.( ces8 des ces |
      bes4. ces8 des ces |
      ges'2. | ges |
      <bes, des ges>2.)\arpeggio
    }
    \\
    \relative c' {
      ges2. | ges |
      ges4. aes8 bes aes |
      ges4. aes8 bes aes |
    }
  >>
}

e_lower = \relative c {
  ges16( des' ges bes des8 es f es) |
  ges,,16( des' ges bes des8 bes bes des) |
  ces,,16( ges' ces es f aes f es des4) |
  ges,16( des' ges bes des8 es f es) |
  ges,,16( des' ges bes des8 bes bes des) |
  \slurDown
  ces,,16( ges' ces es f aes f es des4 <des aes'>\arpeggio |
  <ges, des' ges>4.)\arpeggio
  \slurNeutral
  <ges' des'>8 <ges des'>( <f bes> |
  <ces ges'>4.) <es bes'>8 <es bes'>( <ces ges'> |
  <ges' des'>4.) <ges des'>8 <ges des'>( <f bes> |
  <ces ges'>4.) r8 <es, bes'>4-- |
  <des aes'>2.-- |
  <ces ges'>2. |
  <des aes'>2. |
  <ces ges'>4 <ces'' ges'>2 |
  <aes, es'>4 <aes' es'>2 |
  <f, ces'>4 <f' ces'>2 |
  \slurDown
  <ges, des'>4.( es'8 f es |
  <ges, des'>4. es'8 f es |
  <ges, des'>4. ces8 des ces |
  <ges bes>4. ces8 des ces |
  <ges des' ges>2.)\arpeggio |
}
