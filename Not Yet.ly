\version "2.19.83"

\include "includes/darling_solo_piano.ly"
\include "includes/darling_piano_functions.ly"
\include "A.ly"
\include "B.ly"
\include "C.ly"
\include "D.ly"
\include "E.ly"

\header {
  title = "Not Yet"
  opus = "Oct. 2007"
}

upper = {
  \inits
  \a_upper
  \b_upper
  \c_upper
  \d_upper
  \e_upper
  \bar "|."
}

lower = {
  \a_lower
  \b_lower
  \c_lower
  \d_lower
  \e_lower
}

dynamics = {
  \inits
  %a
  s2.\mp | s2.*5 |
  s4. s4.\p | s2.*2 |
  s4. s4.\< | s4 s2\! |
  s2.\dim s2.*3 |
  
  %b
  s2.\mp s2. | s2. s2 s8 |
  s2. s2. s4. | s2.\< s4. s4.\! |
  s2. | s2.\> s2. | s2.\p s2. |
  s2. s2 s8 | s2. s2. s4. |
  s2.\< s4. s4.\! | s2. |
  s2.\> s2.
  
  %c
  s2.\pp | s2.*7 |
  s2.\p | s2.*3 |
  s2.\dim | s2.*4
  
  %d
  s2.\f s2. s2. s2. s2.
  s2. s2. s2. s2.
  s2. s2.\< s2.\> s2.\mf
  s2.\< s2.\f s2.
  s4. s4.\< s2.\ff\>
  
  %e
  s2.\f s2.*4 s1 |
  s4. s4.\p
  s2.*2 s2 s4\sf s2.\sf
  s2.\dim s2.*4 |
  s2.\mp\dim s2.\!
}

\score {
  \new PianoStaff << 
    \set PianoStaff.connectArpeggios = ##t
    \new Staff = "upper" {\upper}
    \new Dynamics \dynamics
    \new Staff = "lower" {\lower}
  >>
  \layout { }
  \midi { \tempo 4 = 120 }
}
