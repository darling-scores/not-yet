\version "2.19.83"

c_init = {
  \key c \major
  \time 3/4
  \tempo "A tempo"
}

c_upper = \relative c'''{
  \c_init
  <c a>4.( d8 e d |
  <c a>4. a8 a c |
  <b gis e>4.\arpeggio e,8 fis gis |
  <b gis e>4.\arpeggio e,8 fis gis |
  <c a>4. d8 e d |
  <c a>4. a8 a c |
  <b gis e>4.\arpeggio cis8 b cis |
  <b gis e>2.)\arpeggio |
  <f a>4. <a c>8 <a c>( <f a> |
  <e g>4.) <c e>8 <c e>( <e g> |
  <f a>4.) <a c>8 <a c>(-- <b d>) |
  <b d>2--\breathe r8 <c e> |
  <c e>4. <a c>8 <a c>( <c e> |
  <b d>4.) <g b>8 <g b>( <b d> |
  <a c>4.) <f a>8 <f a>( <a c> |
  <g bes>4.)^\markup \italic {rit.} <es g>8 <es g>( <g bes> |
  <f as>4.) <des f>8 <des f>( <f as>) |
  \bar "||"
}

c_lower = \relative c' {
  \c_init
  \clef treble
  a16(_\markup \italic {una corda} e' a c e8 f g f) | 
  a,,16( e' a c e8 c c e) |
  e,,16( b' e gis b2) |
  e,,16( b' e gis b2) |
  a,16( e' a c e8 f g f) | 
  a,,16( e' a c e8 c c e) |
  e,,16( b' e gis b8 cis b cis) |
  b2. |
  f,16( c' f a c2) |
  c,,16( g' c e g2) |
  f,16( c' f a c2) |
  b,16( d g b d2) |
  a,16( e' a c e2) |
  g,,16( d' g b d2) |
  f,,16( c' f a c2) |
  es,,16( bes' es g bes2) |
  des,,16( aes' des f aes2) |
}
