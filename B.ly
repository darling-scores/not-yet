\version "2.19.83"

b_upper = \relative c'' {
  \time 12/8
  \tempo "Piu mosso"
  <<
    {f8(\arpeggio es f) es(\arpeggio des es)}
    \\
    {<c aes>4.\arpeggio bes\arpeggio}
  >>
  des4.( bes) |
  \time 11/8
  <des aes>8(\arpeggio[ <es bes> <des aes>)] <es bes>\arpeggio([ 
  <des aes> <es bes>)] <f c aes>4.\arpeggio r4 |
  \time 15/8
  <<
    {f8(\arpeggio es es) bes'(\arpeggio aes aes)}
    \\
    {<c, aes>4.\arpeggio <f c>\arpeggio}
  >>
  <f des>8( des des es2) r8 ges( |
  \time 12/8
  es4) ges8( es4) ges8( aes2) r8 ges,8( |
  \time 6/8
  es4) ges8( es4) ges8 |
  \time 12/8
  <<
    {bes( aes) aes4( ges8 f es2.) |}
    \\
    {es1. |}
  >>
  <<
    {f'8(\arpeggio es f) es(\arpeggio des es)}
    \\
    {<c aes>4.\arpeggio bes\arpeggio}
  >>
  des4.( bes) |
  \time 11/8
  <des aes>8(\arpeggio[ <es bes> <des aes>)] <es bes>\arpeggio([ 
  <des aes> <es bes>)] <f c aes>4.\arpeggio r4 |
  \time 15/8
  <<
    {f8(\arpeggio es es) bes'(\arpeggio c des)}
    \\
    {<c, aes>4.\arpeggio <f c>\arpeggio}
  >>
  <c' aes>8( aes aes <bes ges>2) r8 des8( |
  \time 12/8
  bes4) des8( bes4) des8( es2) r8
  \ottava #1
  ges8( | 
  \time 6/8
  es4) ges8( es4) ges8 |
  \time 12/8
  <<
    {bes(^\markup \italic {rit.} aes) aes4( ges8 f es4. des) |}
    \\
    {es1. |}
  >>
  \ottava #0
  \bar "||"
}

b_lower = \relative c' {
  \time 12/8
  \repeat unfold 2 {
    <f des bes>4.\arpeggio <ges es ces>\arpeggio <aes f des> <ges es> |
    \time 11/8
    <f des>8[(\arpeggio <ges es ces> <f des>)] <ges es ces>[(\arpeggio 
    <f des> <ges es ces>)] <f des bes>4.\arpeggio r4 |
    \time 15/8
    <f des bes>4.\arpeggio <ges es ces>\arpeggio <aes f des> <bes ges es>2 
    r8 <des bes ges>( |
    \time 12/8
    <bes ges es>4) <des bes ges>8( <bes ges es>4) <des bes ges>8 <es c aes>2 
    r8 <des, bes ges>( |
    \time 6/8
    <bes ges es>4) <des bes ges>8( <bes ges es>4) <des bes ges>8 |
    \time 12/8
  }
  \alternative {
    {<ces aes>1.}
    {<es ces aes>1.}
  }
}
