\version "2.18.2"

\paper {
  #(set-paper-size "letter")
}

\header {
  composer = "Caleb Darling"
  copyright = \markup {Copyright \char ##x00A9 2019}
  tagline = ##f
}

inits = {
  \override DynamicTextSpanner.style = #'none
}