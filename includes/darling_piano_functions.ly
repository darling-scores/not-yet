\version "2.18.2"

sl = \change Staff = "lower"
su = \change Staff = "upper"

octaves = #(define-music-function
             (parser location relative passage)
             (ly:pitch? ly:music?)
           #{
             \new Voice <<
               \relative #relative $passage
               \transpose c c, {\relative #relative { $passage }}
             >>
           #})