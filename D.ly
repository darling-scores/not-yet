\version "2.19.83"

d_init = {
  \key ges \major
  \clef bass
}

d_upper = \relative c' {
  \d_init
  <<
    {es2( es8 des | des2.) |
     ges2( ges8 f | f2.) |
     ges8( f) f4( es8 des) |}
    \\
    {ges,4. f8 aes4 | bes2. |
     bes4. aes8 c4 | des2. |
     c |}
  >>
  <f a,>8( des) <f bes,>2 |
  <es, as c>8( as) ges4 f | es2.-- |
  <<
    {es'2( es8 des | des2.) |}
    \\
    {ges,4. f8 aes4 | bes2. |}
  >>
  r4 ges'( ges8 f | f2.) |
  <<
    {f8[( es])}
    \\
    {<des bes>4}
  >>
  es4( des8 c | bes2) a4( |
  <f' des bes>4.) <f, des bes>8 <f des bes>( <ges es ces>) | 
  <ges es ces>2 <ges' es ces>4 |
  \ottava #1
  \set Staff.ottavation = #"8"
  <f' des bes>4. 
  \ottava #0
  <f,, des bes>8 <f des bes>(-- <ges es ces>) | 
  <ges es ces>2-- <ges' es ces>4 |
}

d_lower = \relative c {
  \d_init
  <<
    \relative c {es4 es}
    \\
    \relative c, {es2_\markup \italic {tre corde}}
  >>
  <f f,>4 |
  <ges ges,>2. |
  <<
    \relative c' {ges4 ges}
    \\
    \relative c {ges2}
  >>
  <aes aes,>4 |
  <bes bes,>2~ <bes bes,>8 <aes aes,> |
  <ges ges,>2. |
  <f f,>4 <des des,> <bes bes,> |
  <aes aes,>2. |
  <es es,>-- |
    <<
    \relative c {es4 es}
    \\
    \relative c, {es2}
  >>
  <f' f,>4 |
  <<
    \relative c' {<ges ges,>2. | r4 <bes ges>4 <c aes> | <des bes>2. | }
    \\
    \relative c {s2 s8 <es, es,>16 <es es,> | <es es,>2.-> | r2 r8 <aes' aes,> | }
  >>
  <ges ges,>2.~ | <ges ges,>2 <f f,>4 |
  <bes, bes,>4. <f bes,>8 <f bes,>( <ges ces,>) | 
  <ges ces,>2 <ges' ces,>4 |
  <f bes,>4. <f, bes,>8 <f bes,>--( <ges ces,>) | 
  <ges ces,>2-- <ges' ces,>4 |
}
